# Moneltery

## 1.1.1. Projektindító dokumentum

Ez egy raktári bevételezést segítő (webes)applikáió.

Bejelentkezés után a felhasználónak lehetősége nyílik a 
készlet adatainak szerkesztésére, a mennyiség vagy az ár módostására.

Egy meglévő termék törléséhez, vagy új áru hozzáadásához admin 
jogosultság szükséges.

Új termék felvételéhez meg kell adni annak nevét, mennyiségét,
kategóriáját, és árát.

Lehetőség van a kategóriák szerint kilistázni a árut.

## 1.1.3. Nem funkcionális elvárások

- Felhasználóbarát, egyszerű, letisztult felület
- Jelszavas azonosítás, jelszavak biztonságos tárolása
- Termékek módosítása biztonságos elrejtése a látogatók elől

## 1.1.2. Funkcionális elvárások

- Bejelentkezés
- Felhasználók listázása
- Termékek kezelése (létrehozás, törlés, módosítás)
- Termékek kategorizálása címke alapján
- Termékek listázása

#### Tagok által elérhető funkciók

- Termékek bevitele
- Termékek megtekintése
- Egy termékhez tartozhat:
    * id
    * név
    * mennyiség
    * kategória
    * ár
    * dátum


#### Adminisztrátor

- Termékek kezelése (létrehozás, törlés, módosítás)
- Felhasználók törlése

## 1.1.4. Fejlesztői környezet

#### Ajánlott IDE: IntelliJ IDEA

- Bitbucket account nem szükséges.
- A projektet közvetlenül az oldalról is letölthetjük.
- Ezek után szükséges még az adatok egy választott mappába történő kicsomagolása.

- Indítsuk el a fejlesztő környezetet, majd az "Import project" lehetőség kiválasztása után navigáljunk a projektünkhöz, 
  majd azt kiválaszva haladjunk tovább.
- A következő ablakon az "Import project from external source" és "Maven" lehetőséget kiválasztva haladjunk tovább.
- Ezután a "JDK for importer" menüből a java 1.8-as verzióját kell kiválasztani.
- A következő oldalakon már csak tovább kell lépnünk.

- Egy rövid töltési fázist követően a "moneltery > Plugins > spring-boot > spring-boot:run" lehetőséget kiválasztva tudjuk elindítani a programot.
    
#### Végpontok teszteléséhet: Advanced REST client

- Futtassuk a programot IntelliJ-ben
- Az ARC-ben a metódushhoz váasszuk ki tesztelendó végpont típusát (GET,POST,PUT,DELET)
- A Request URL mezőbe a `http://localhost:8080/`[tesztelni kívánt végpont]


## 1.1.5. Adatbázis-terv
![UML diagram](https://bitbucket.org/bivan-elte/moneltery/raw/master/docs/images/uml.png)

## 1.1.6. Könyvtárstruktúra
#### hu.elte.moneltery.controllers
- LabelController.java
- ProductController.java
- UserController.java

#### hu.elte.moneltery.entities
- Label.java
- Product.java
- User.java

#### hu.elte.moneltery.repositories
- LabelRepository.java
- ProuctRepository.java
- UserRepository.java

#### hu.elte.moneltery.security
- AuthenticatedUser.java
- MyUserDetailsService.java
- WebSecurityConfig.java

## 1.1.7. Végpontok

#### GET/
- `/users`: felhasználók és adataik kilistázása (admin)
- `/products`: termékek kilistázása
- `/products/{id}`: termék kilistázása id alapján
#### POST/
- `/users/login`: bejelentkezés
- `/users/register`: regisztráció
- `/products`: új termék létrehozása
#### PUT/
- `/products/{id}`: termékadat módosítása
#### DELETE/
- `/users/{id}`: felhasználó törlése (admin)
- `/products/{id}`: termék törlése


#### 1.1.8 Use-case modell
![Use-case diagram](https://bitbucket.org/bivan-elte/moneltery/raw/master/docs/images/usecase.jpg)

#### 1.1.9 Product törlés folyamata
![Sequence diagram](https://bitbucket.org/bivan-elte/moneltery/raw/master/docs/images/sequence.png)