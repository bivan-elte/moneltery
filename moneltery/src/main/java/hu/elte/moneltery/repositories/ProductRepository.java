package hu.elte.moneltery.repositories;

import hu.elte.moneltery.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    Iterable<Product> findByName(String title);
}
