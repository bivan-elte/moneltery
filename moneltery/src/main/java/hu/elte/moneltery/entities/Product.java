package hu.elte.moneltery.entities;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private String name;

    @Column
    private Integer amount;

    @Column
    private String category;

    @Column
    private Integer price;

    @Column(updatable = false)
    @NotNull
    @CreationTimestamp
    private LocalDateTime createdAt;


    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private User user;
}
