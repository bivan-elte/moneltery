package hu.elte.moneltery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonelteryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonelteryApplication.class, args);
	}

}
