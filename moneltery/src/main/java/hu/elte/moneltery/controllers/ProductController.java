package hu.elte.moneltery.controllers;

import hu.elte.moneltery.entities.Product;
import hu.elte.moneltery.repositories.ProductRepository;
import hu.elte.moneltery.security.AuthenticatedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @Autowired
    private ProductRepository productRepositroy;


    // List products
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("")
    public Iterable<Product> getProducts() {
        return productRepositroy.findAll();
    }

    // List product by id
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/{id}")
    public Optional<Product> getProductsById(
            @PathVariable Integer id
    ) {
        return productRepositroy.findById(id);
    }

    // Update product
    @Secured({"ROLE_ADMIN"})
    @PutMapping("/{id}")
    public ResponseEntity<Product> update
            (@PathVariable Integer id,
             @RequestBody Product product) {
        Optional<Product> oProduct = productRepositroy.findById(id);
        if (oProduct.isPresent()) {
            product.setId(id);
            return ResponseEntity.ok(productRepositroy.save(product));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Create product
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping("")
    public ResponseEntity<Product> createProduct(
            @RequestBody Product product
    ) {
        product.setCreatedAt(LocalDateTime.now());
        product.setUser(authenticatedUser.getUser());
        Product savedProduct = productRepositroy.save(product);
        return ResponseEntity.ok(savedProduct);
    }

    // Delete product
    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("/{id}")
    public ResponseEntity deleteProduct(
            @PathVariable Integer id
    ) {
        try {
            productRepositroy.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

//    @PutMapping("/{id}/labels")
//    public ResponseEntity<Label> modifyLabels
//            (@PathVariable Integer id,
//             @RequestBody Label label) {
//        Optional<Product> oProduct = productRepositroy.findById(id);
//        if (oProduct.isPresent()) {
//            Product product = oProduct.get();
//            System.out.println("Products: " + product);
//
//            if (label.getId() == null) {
//                System.out.println("LABEL: " + label);
//                labelRepository.save(label);
//            }
//
//            product.setLabel(label);
//            productRepositroy.save(product);
//            return ResponseEntity.ok(label);
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }
}
