INSERT INTO user (name, username, password, enabled, role) VALUES ('Tibor', 'tibor', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', true, 'ROLE_ADMIN');
INSERT INTO user (name, username, password, enabled, role) VALUES ('Benedek', 'bivan', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', true, 'ROLE_ADMIN');
INSERT INTO user (name, username, password, enabled, role) VALUES ('László', 'laci', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', true, 'ROLE_USER');



INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Lenovo', 2, 'laptop', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Acer', 2, 'laptop', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('HP', 2, 'laptop', 10000, CURRENT_TIMESTAMP());

INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Mouse', 50, 'accessories', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Keyboard', 250, 'accessories', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Headset', 56, 'accessories', 10000, CURRENT_TIMESTAMP());

INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('XBOX', 9654, 'console', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('PS4', 9000, 'console', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Switch', 900, 'console', 10000, CURRENT_TIMESTAMP());
INSERT INTO product (name, amount, category, price, created_at)
    VALUES ('Wii', 1, 'console', 10000, CURRENT_TIMESTAMP());




